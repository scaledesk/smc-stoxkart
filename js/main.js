
(function($){
  "use strict";


  // for hide and show homepage form field
  $(document).ready(function(){
    $('[data-otp], [data-password], [data-forgot-passowrd], [data-message]').hide();
    $('[data-submit]').on('click', function() {
        $('[data-otp], [data-email]').show();
        $('[data-password],[data-forgot-passowrd], [data-message]').hide();
    });
    $('[data-resume-application]').on('click', function() {
        $('[data-password], [data-forgot-passowrd]').show();
        $('[data-email], [data-otp], [data-message]').hide();
    });

    $('[data-forgot-passowrd]').on('click', function() {
        $('[data-message], [data-mobile]').show();
        $('[data-password], [data-forgot-passowrd]').hide();
    });
  });
// for hide and show homepage form field

    // video record
    $('[data-record-video]').hide();
    $('[data-record-btn]').on('click', function() {
        $('[data-record-video]').show();
        $('[data-record-btn], [data-uplpad-video], [data-or-hide], [data-recorded]').hide();
    });
    $('[data-play]').on('click', function() {
        $('[data-recorded]').show();
        $('[data-camera]').hide();
    });
    $('[data-record]').on('click', function() {
        $('[data-camera]').show();
        $('[data-recorded]').hide();
    });


  $('form').on('submit', function(e) {
    e.preventDefault();
    if($(this).valid()) {
      $('#panModal').modal();
    }
  });

  // email validation
  $(" #email").on({ "change": function(e) {
        var userinput = $("#email").val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(userinput)

        if(!pattern){
            e.preventDefault();
            $('.email-error').show();
            $("#email").addClass('error-input');
            $('.email-error').text('This is not valid email')
          }
          else{
            $("#email").removeClass('error-input');
            $('.email-error').hide();
            $(this).submit();
          }
          },
        });

        // mobile validation
        $("#number").on({ "change": function(e) {
              var userinput = $("#number").val();
              var pattern = /^[6-9]\d{9}$/i.test(userinput)

              if(!pattern){
                  e.preventDefault();
                  $('.number-error').show();
                  $("#number").addClass('error-input');
                  $('.number-error').text('This is not valid number')
                }
                else{
                  $("#number").removeClass('error-input');
                  $('.number-error').hide();
                  $(this).submit();
                }
                },
              });
})(jQuery)

$(document).ready(function(){
    $( ".input-file" ).keydown( function( event ) {
        if ( event.keyCode == 13 || event.keyCode == 32 ) {
            $( ".input-file" ).focus();
        }
    });
    $( ".input-file-trigger" ).click(function( event ) {
      $( ".input-file" ).focus();
      return false;
    });
    $( ".input-file").change( function( event ) {
        $(this).siblings('.file-return').text($(this).val());
    });

    $(".check").click(function () {
        if ($(this).is(":checked")) {
            $(this).parents().siblings(".hidden-field").hide();
        } else {
            $(this).parents().siblings(".hidden-field").show();
        }
    });
    $(".yes").click(function () {

        if ($(this).is(":checked")) {
            $(this).parents().siblings(".hidden-field").hide();
        }
    });
    $(".no").click(function () {

        if ($(this).is(":checked")) {
            $(this).parents().siblings(".hidden-field").show();
        }
    });
  });


$(document).ready(function () {

      $('.do-validate').validate({
          rules:
          {
              'radio':{ required:true },
              'scheme-select':{ required:true },
              'myfile-1': {
                  required: true,
              },
          },
           messages:
          {
              'myfile-1': {
                  required: 'Please upload a file',
              },
              'occupation': {
                  required: 'Please select occupation',
              },
              'mother-name':
              {
                  required:"Please fill mother's name"
              },
              'father-name':
              {
                  required:"Please fill father's name/spouse's name"
              },
              'tin':
              {
                  required:"Please fill TIN number"
              },
              'cor':
              {
                  required:"Please fill country of residence"
              },
              'cob':
              {
                  required:"Please fill country of birth"
              },
              'coj':
              {
                  required:"Please fill country of jurisdiction"
              },
              'radio':
              {
                  required:"Please select anyone option",
              },
              'address-1':
              {
                  required:"Please fill address line 1"
              },
              'address-2':
              {
                  required:"Please fill address line 2"
              },
              'address-3':
              {
                  required:"Please fill address line 3"
              },
              'pan':
              {
                  required:"Please enter 10 digits for a valid PAN number"
              },
              'pan-detail':
              {
                  required:"Please fill the PAN card number"
              },
              'dob':
              {
                  required:"Please fill the date of birth"
              },
              'acc-number':
              {
                  required:'Please fill the account number'
              },
              'ifsc':
              {
                  required:'Please fill the IFSC code'
              },
              'micr':
              {
                  required:'Please fill the MICR code'
              },
              'experience':
              {
                      required:'Please select experience level'
              },
          },
          errorPlacement: function (error, element) {
                  if (element.attr("type") == "text") {
                      error.insertAfter($(element).parents('label'));
                  }
                  if (element.attr("type") == "file") {
                      error.insertAfter($(element).parents('.input-file-container'));
                  }
                  if ( element.is(":radio") )
                  {
                      error.appendTo( element.parents().find('.radio-row') );
                  }
                  if ( element.is('select'))
                  {
                      error.appendTo( element.parents('.form-group'));
                  }
          },
      });

});

/************exit intent*************** */

jQuery(function(){ // on DOM Page Load
    ddexitpop.init({
        contentsource: ['inline', '<div id="ddexitpop1" class="ddexitpop"><div class="container text-center py-4 content"><div class="row"><div class="py-4 col-12"><img style="max-width:200px;" class="m-auto img-fluid" src="img/exit-intent.png" alt="img"><h2 class="text-capitalize font-weight-bold">Exit intent <span class="text-primary">lorem ipsum</span></h2><p class="font-size-16 mb-4 font-weight-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div></div></div><a class="calltoaction border border-light intent-popup-close-btn" href="#" onClick="ddexitpop.hidepopup()">X</a></div>;'],
        fxclass: 'random',
        hideaftershow: false,
        displayfreq: 'always',
        onddexitpop: function($popup){
            console.log('Exit Pop Animation Class Name: ' + ddexitpop.settings.fxclass)
        }
    });
});

    //   bioEp.init({
    //     html: '<div class="container text-center py-4 content"><div class="row"><div class="py-4 col-12"><img style="max-width:200px;" class="m-auto img-fluid" src="img/exit-intent.png" alt="img"><h2 class="text-capitalize font-weight-bold">Exit intent <span class="text-primary">lorem ipsum</span></h2><p class="font-size-16 mb-4 font-weight-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div></div></div>',
    //     css: '#content {font-family: "Titillium Web", sans-serif; font-size: 14px;}',
	// 	    cookieExp: 0
    //   });
